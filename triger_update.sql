use i_b_module_3
go


drop trigger if exists my_first_tr
go

-- first trigger

CREATE TRIGGER my_first_tr ON I_B_module_3.product 
AFTER UPDATE
AS   
IF UPDATE ([maker])  
BEGIN  
UPDATE i_b_module_3.product
SET updated_date = GETDATE()
WHERE id_product in  (SELECT id_product  FROM Inserted);   
END;  
GO  

-- перевірка трігера

update i_b_module_3.product
SET maker = 'sony'  
WHERE id_product = 3;  
GO  

/*++++++++++*/

select laptop.model, laptop.serial, product.maker 
from i_b_module_3.laptop JOIN i_b_module_3.product ON laptop.id_laptop=product.id_laptop
go