use i_b_module_3
go

-- view product

CREATE VIEW [my_view_product] AS
SELECT [type],
       [maker],
       [supplier],
       [invoice],
       [date],
       [city],
       [address],
       [postal_code],
       [inserted_date],
       [updated_date]
       FROM [product]
GO