
CREATE DATABASE i_blok_library_view
GO

USE i_blok_library_view
GO

--*******VIEW VIEW VIEW********

CREATE VIEW [my_view_authors_log] AS
SELECT * FROM [i_blok_library].[dbo].[authors_log]
GO  

--***********
CREATE VIEW [my_view_publisher] AS
SELECT * FROM [i_blok_library].[dbo].[publisher]
GO
--***********
CREATE VIEW [my_view_authors] AS
SELECT * FROM [i_blok_library].[dbo].[authors]
GO
--***********
CREATE VIEW [my_view_books] AS
SELECT * FROM [i_blok_library].[dbo].[books]
GO
--***********
CREATE VIEW [my_view_books_a] AS
SELECT * FROM [i_blok_library].[dbo].[books_authors]
GO

SELECT * FROM my_view_authors_log
GO