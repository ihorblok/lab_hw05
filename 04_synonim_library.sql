--********synonim***********
CREATE DATABASE i_blok_library_synonim
GO

USE i_blok_library_synonim
GO

-- create synonim--

CREATE SYNONYM  authors_log_syn for [i_blok_library].[dbo].[authors_log]
GO

CREATE SYNONYM  publisher_syn for [i_blok_library].[dbo].[publisher]
GO

CREATE SYNONYM  authors_syn for [i_blok_library].[dbo].[authors]
GO

CREATE SYNONYM  books_syn for [i_blok_library].[dbo].[books]
GO

CREATE SYNONYM books_authors_syn for [i_blok_library].[dbo].[booksauthors]
GO

SELECT * FROM authors_log_syn
GO