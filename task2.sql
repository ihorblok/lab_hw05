use i_b_module_3
go
-- drop tables
drop TABLE if exists [server]
GO

DROP TABLE if exists [pc_table];
GO
-- create [server]
CREATE TABLE [server]
(
 [id_server]        INT NOT NULL IDENTITY (1, 1),
 [processor]        TEXT NOT NULL ,
 [ram]              TEXT NOT NULL ,
 [hdd]              int NULL DEFAULT (0),
 [dvd_r]            TEXT NULL ,
 [screen]           NUMERIC (18,1) NULL check ([screen] >= 0),
 [ip_address]       TEXT NULL ,
 [mac_address]      TEXT NULL ,
 [ovner]            TEXT NOT NULL ,
 [address_building] TEXT NULL ,
 [office]           TEXT NULL ,
 [email]            TEXT NULL ,

 CONSTRAINT [PK_server] PRIMARY KEY CLUSTERED ([id_server])
);
GO

-- create [PC]
CREATE TABLE [pc_table]
(
 [id_pc]            INT NOT NULL IDENTITY (1,1),
 [processor]        TEXT NOT NULL ,
 [ram]              TEXT NOT NULL ,
 [hdd]              INT NULL DEFAULT (0),
 [dvd_r]            TEXT NULL ,
 [screen]           NUMERIC(18,1) NULL check ([screen]>= 0),
 [ip_address]       TEXT NULL ,
 [mac_address]      TEXT NULL ,
 [ovner]            TEXT NOT NULL ,
 [address_building] TEXT NULL ,
 [office]           TEXT NULL ,
 [email]            TEXT NOT NULL ,
 [type_operation]   TEXT NOT NULL ,
 [date_operation]   DATE NOT NULL ,

 CONSTRAINT [PK_pc_table] PRIMARY KEY CLUSTERED ([id_pc])
);
GO
-- trigger update, insert, delete

DROP trigger if exists pc_table_insert
GO


CREATE TRIGGER pc_table_update
    ON [pc_table]
    AFTER UPDATE
    AS 
        BEGIN
            raiserror('Error delete', 16, 1)
            rollback transaction
        END
    GO  

CREATE TRIGGER pc_table_insert
    ON [pc_table]
    AFTER INSERT
    AS 
        BEGIN
            raiserror('Error insert!', 16, 1)
            rollback transaction
        END
    GO


CREATE TRIGGER pc_table_delete
    ON [pc_table]
    AFTER DELETE
    AS 
        BEGIN
            raiserror('Error delete', 16, 1)
            rollback transaction
        END
    GO

-- insert pc_table
INSERT INTO [pc_table]
		(
        [processor],
        [ram],
        [hdd],
        [dvd_r],
        [screen],
        [ip_address],
        [mac_address],
        [ovner],
        [address_building],
        [office],
        [email],
        [type_operation],
        [date_operation])	  
VALUES
    ('core i3 3.1 GHz', '4G', 500, NULL, 22, '192.168.1.5', '00:08:E3:B0:9B:80', 'Petrenko I.G.', 'Mechnikova, 25', '206k.', 'qwer@ukr.net', 'fix screen', '04-05-2018'),
    ('core i5 3.6 GHz', '8G', 1000, NULL, 22, '192.168.1.6', '00:08:E3:B0:9B:81', 'Schur P.V.', 'Mechnikova, 25', '312k.', 'schur@ukr.net', 'fix keyboard', '05-05-2018')
GO                                         
-- select

 SELECT * FROM pc_table
 go

 -- insert server
INSERT INTO [server]
		(
        [processor],
        [ram],
        [hdd],
        [dvd_r],
        [screen],
        [ip_address],
        [mac_address],
        [ovner],
        [address_building],
        [office],
        [email]) 
VALUES
    ('Xeon E7-4800 3.2 GHz', '2T', 10000, NULL, 22, '192.168.1.100', 'E0:08:E3:15:9B:80', 'Kril I.G.', 'Stryjska, 105', '107k', 'qwer@ukr.net'),
    ('Xeon E7-8800 3.2 GHz', '2T', 10000, NULL, 22, '192.168.1.101', '15:05:E3:B0:9B:81', 'Kril I.G.', 'stryjska, 105', '107k', 'schur@ukr.net')
GO                                         
-- select
 SELECT * FROM [server]
 go

-- schema

CREATE SCHEMA education
GO

CREATE synonym education.i_blok_server for i_b_module_3.dbo.server
GO

CREATE synonym education.i_blok_pc_table for i_b_module_3.dbo.pc_table
GO

-- check all

SELECT * FROM education.i_blok_pc_table
GO

SELECT * FROM education.i_blok_server
GO

SELECT * FROM education.i_blok_pc_table
WHERE id_pc = 2
GO

SELECT ram, hdd, processor, ip_address 
FROM education.i_blok_server
WHERE id_server = 2
GO

UPDATE pc_table
SET hdd = 5000  
WHERE id_pc=4;  

GO
DELETE pc_table 
WHERE id_pc=4;  
GO