USE i_blok_library
GO

INSERT into [authors] ([author_id], [name], [url], [inserted], [inserted_by])
VALUES   (next value for library_seq, 'Stiven King', 'StivenKing.com', GETDATE(), user_name()),
        (next value for library_seq, 'Suzanne Collins', 'SuzanneCollins.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'J.K. Rowling', 'rowling.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Jane Austen', 'Janeausten.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'J.R.R. Tolkien', 'Tolkien.com', GETDATE(), user_name()),
        (next value for library_seq, 'Alexandre Dumas', 'dumas.com', GETDATE(), user_name()),
        (next value for library_seq, 'George R.R. Martin', 'gmartin.com', GETDATE(), user_name()),
        (next value for library_seq, 'Harper Lee', 'hlee.com', GETDATE(), user_name()),
        (next value for library_seq, 'Antoine de Saint-Exupery', 'saint-exupery.com', GETDATE(), user_name()),
        (next value for library_seq, 'Voltaire', 'voltaire.com', GETDATE(), user_name()),
        (next value for library_seq, 'Stieg Larsson', 'larsson.com', GETDATE(), user_name()),
        (next value for library_seq, 'Paulo Coelho', 'pcoelho.com', GETDATE(), user_name()),
        (next value for library_seq, 'Andy Weir', 'andyweir.com', GETDATE(), user_name()),
        (next value for library_seq, 'Michael Crichton', 'mcrichton.com', GETDATE(), user_name()),
        (next value for library_seq, 'Jo Nesbo', 'jnesbo.com', GETDATE(), user_name()),
        (next value for library_seq, 'Agatha Christie', 'achristie.com', GETDATE(), user_name()),
        (next value for library_seq, 'George Orwell', 'gcrwell.com', GETDATE(), user_name()),
        (next value for library_seq, 'Shel Silverstein', 'ssilverstein.com', GETDATE(), user_name()),
        (next value for library_seq, 'Brandon Sanderson', 'sbanderson.com', GETDATE(), user_name()),
        (next value for library_seq, 'Philip Pullman', 'ppullman.com', GETDATE(), user_name())

GO


INSERT into [publisher] ([publisher_id], [name], [url], [inserted], [inserted_by])
VALUES  (next value for library_seq, 'Veselka', 'veselka.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Bison Books', 'bisonbooks.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Black Dog Publishing', 'blackdog.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Chronicle Books', 'chronicle.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Deseret Book', 'DeseretBook.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Dedalus Books', 'DedalusBooks.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Copper Canyon Press', 'CopperCanyonPress.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'DNA Publications', 'DNAPublications.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Cisco Press', 'CiscoPress.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Collins', 'Collins.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Cassell', 'Cassell.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'FabJob', 'FabJob.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Grafton', 'Grafton.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Hyperion', 'Hyperion.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'HarperCollins', 'HarperCollins.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Longman', 'Longman.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'NavPress', 'NavPress.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Newnes', 'Newnes.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Parragon', 'Parragon.com', GETDATE(), USER_NAME()),
        (next value for library_seq, 'Quebecor', 'Quebecor.com', GETDATE(), USER_NAME())
GO

INSERT into [books] ([isbn], [publisher_id], [url], [price], [inserted], [inserted_by])
VALUES  ('98465189161', 21, 'veselka.com', 21.50, GETDATE(), USER_NAME()),
        ('51551844844', 22, 'bisonbookss.com', 12.60, GETDATE(), USER_NAME()),
        ('46385005271', 23, 'blackdog.com', 14.60, GETDATE(), USER_NAME()),
        ('42274843792', 24, 'chronicle.com', 12.60, GETDATE(), USER_NAME()),
        ('89188188106', 25, 'DeseretBook.com', 23.60, GETDATE(), USER_NAME()),
        ('84549687580', 26, 'DedalusBooks.com', 15.80, GETDATE(), USER_NAME()),
        ('95429574411', 27, 'CopperCanyonPress.com', 11.60, GETDATE(), USER_NAME()),
        ('15484845689', 28, 'DNAPublications.com', 5.90, GETDATE(), USER_NAME()),
        ('28844494990', 29, 'CiscoPress.com', 17.56, GETDATE(), USER_NAME()),
        ('15989949843', 30, 'Collins.com', 19.32, GETDATE(), USER_NAME()),
        ('31354046967', 31, 'Cassell.com', 45.26, GETDATE(), USER_NAME()),
        ('49719140451', 32, 'FabJob.com', 12.84, GETDATE(), USER_NAME()),
        ('68084233935', 33, 'Grafton.com', 21.95, GETDATE(), USER_NAME()),
        ('86449327419', 34, 'Hyperion.com', 17.92, GETDATE(), USER_NAME()),
        ('10481442095', 35, 'HarperCollins.com', 23.00, GETDATE(), USER_NAME()),
        ('12317951438', 36, 'Longman.com', 16.75, GETDATE(), USER_NAME()),
        ('14154460787', 37, 'NavPress.com', 18.46, GETDATE(), USER_NAME()),
        ('15990970136', 38, 'Newnes.com', 13.15, GETDATE(), USER_NAME()),
        ('17827479485', 39, 'Parragon.com', 18.32, GETDATE(), USER_NAME()),
        ('19663988834', 40, 'Quebecor.com', 19.45, GETDATE(), USER_NAME())
GO

INSERT into [booksauthors] ([booksauthors_id], [isbn], [author_id], [seq_no], [inserted], [inserted_by])
VALUES  (1, '98465189161', 1, 1, GETDATE(), USER_NAME()),
        (2, '51551844844', 2, 2, GETDATE(), USER_NAME()),
        (3, '46385005271', 3, 3, GETDATE(), USER_NAME()),
        (4, '42274843792', 4, 4, GETDATE(), USER_NAME()),
        (5, '89188188106', 5, 5, GETDATE(), USER_NAME()),
        (6, '84549687580', 6, 6, GETDATE(), USER_NAME()),
        (7, '95429574411', 7, 7, GETDATE(), USER_NAME()),
        (8, '15484845689', 8, 8, GETDATE(), USER_NAME()),
        (9, '28844494990', 9, 9, GETDATE(), USER_NAME()),
        (10, '15989949843', 10, 10, GETDATE(), USER_NAME()),
        (11, '31354046967', 11, 11, GETDATE(), USER_NAME()),
        (12, '49719140451', 12, 12, GETDATE(), USER_NAME()),
        (13, '68084233935', 13, 13, GETDATE(), USER_NAME()),
        (14, '86449327419', 14, 14, GETDATE(), USER_NAME()),
        (15, '10481442095', 15, 15, GETDATE(), USER_NAME()),
        (16, '12317951438', 16, 16, GETDATE(), USER_NAME()),
        (17, '14154460787', 17, 17, GETDATE(), USER_NAME()),
        (18, '15990970136', 18, 18, GETDATE(), USER_NAME()),
        (19, '17827479485', 19, 19, GETDATE(), USER_NAME()),
        (20, '19663988834', 20, 20, GETDATE(), USER_NAME())
GO

SELECT * FROM authors
GO

SELECT * FROM publisher
GO

SELECT * FROM booksauthors
GO

SELECT * FROM books
GO

SELECT * FROM authors_log
GO
